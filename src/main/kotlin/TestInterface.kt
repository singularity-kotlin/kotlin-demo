interface Named {
    val name: String
}

interface Human : Named {
    val firstName: String
    val lastName: String

    override val name: String get() = "$firstName $lastName"
}
class Position(val position: String = "CEO") {
    override fun toString() = "$position"
}
data class Employee(
    // реализация 'name' не требуется
    override val firstName: String,
    override val lastName: String,
    val position: Position,

) : Human {
    override fun toString() = "$firstName $lastName $position"
}


fun main() {
    val employee = Employee("Max", "Maximillian", Position("CEO"))
    println(employee)
}