
fun main(args: Array<String>){
    val scan = java.util.Scanner(System.`in`)
    val text = scan.nextLine()
    var translation=""
    val latin= arrayOf("a", "e", "q", "p", "f", " ",
        "á", "ё", "l", "r", "h", "y",
        "b", "j", "m", "s", "һ", "і",
        "v", "z", "n", "t", "ts", " ",
        "g", "i", "ń", "ý", "ch", "e",
        "ǵ", "i", "o", "u", "sh", "iý",
        "d", "k", "ó", "ú", "sh", "ia")

    val kiril= arrayOf("а", "е", "қ", "п", "ф", "ъ",
        "ә", "ё", "л", "р", "х", "ы",
        "б", "ж", "м", "с", "һ", "і",
        "в", "з", "н", "т", "ц", "ь",
        "г", "и", "ң", "у", "ч", "э",
        "ғ", "й", "о", "ұ", "ш", "ю",
        "д", "к", "ө", "ү", "щ", "я")
    var index = -1
    var isCapital=false
    for(c in text) {
        if(c.isUpperCase()) {
            index = latin.indexOf(c.lowercase() + "")
            isCapital=true
        } else {
            isCapital=false
            index = latin.indexOf(c + "")
        }
        translation += if(isCapital)
            kiril[index].uppercase()
        else
            kiril[index]
    }

    println(translation)

}