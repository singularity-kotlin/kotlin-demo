import java.util.*

fun main(args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
    var a: Int = 1
    var b: Int = 2
    println("${a}+${b}=${sum(a,b)}")

    val ch: Char = '1';
    val number: Int = ch.digitToInt()

    val d: Double = 1.0;

    val oneMillion = 1_000_000
    val creditCardNumber = 1234_5678_9012_3456L
    val socialSecurityNumber = 999_99_9999L
    val hexBytes = 0xFF_EC_DE_5E
    val bytes = 0b11010010_01101001_10010100_10010010

    println(creditCardNumber)

    println(number is Int)

    val str: String = "hello world".uppercase()
    for(c in str)
        println(c)

    val text = """
    >Tell me and I forget.
    >Teach me and I remember.
    >Involve me and I learn.
    >(Benjamin Franklin)
    """.trimMargin(">")

    println(text)

    val arr: Array<String> = arrayOf("1", "2", "3", "6")
    arr[1]="123"
    arr.set(0, "999")
    arr.forEach { println(it) }

    val intarr: IntArray = intArrayOf(1, 2, 3, 4, 5)
    intarr.forEach { println(it*2) }

    val scan = Scanner(System.`in`)
    //val n = scan.nextDouble()

    //val w = n.toInt()

    val llist = linkedSetOf(1,2,3)
    val slist = sortedSetOf(3,2,4,1,5)
    slist.forEach { println(it) }

    val map = mapOf(123 to "number1", 345 to "number2", 678 to "number3")
    println(map)

    loopsDemo();

    printStrings("1", "Hello:", "My name", "is", "Khan")

    val numbers = intArrayOf(1, 2, 3, 4)
    printIntegerFromArray(*numbers)

    displayUser("Kate")

    /* Higher Order Functions */
    action(5, 3, ::sum)         // 8
    action(5, 3, ::multiply)    // 15
    action(5, 3, ::subtract)

    val initDemo = InitOrderDemo("hello")
}

fun action (n1: Int, n2: Int, op: (Int, Int)-> Int){
    val result = op(n1, n2)
    println(result)
}
fun sum2(a: Int, b: Int): Int{
    return a + b
}
fun subtract(a: Int, b: Int): Int{
    return a - b
}
fun multiply(a: Int, b: Int): Int{
    return a * b
}


fun displayUser(name: String, age: Int = 18, position: String="unemployed"){
    println("Name: $name   Age: $age  Position: $position")
}

fun sum(a: Int, b: Int): Int {
    return a+b;
}

fun loopsDemo() {
    for(i in 1..5){
        println(i)
    }
    val students = arrayListOf<String>()
    students.add("Beibut")
    students.add("Azat")
    students.add("Anara")

    for(student in students){
        println("Student $student")
    }

    for (index in students.indices){
        println("Student ${students[index]}")
    }

    val allStudents = listOf("Beibut","Azat","Dias","Nurbek","Dauren")
    allStudents.forEachIndexed{ index, item ->
        println("$index : $item")
    }

    repeat(5){
        println("I am Android Developer!")
    }
}

fun printStrings(vararg strings: String){
    for(str in strings)
        println(str)
}

fun printIntegerFromArray(vararg numbers: Int) {
    for(i in numbers)
        println(i)
}