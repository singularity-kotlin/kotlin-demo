class InitOrderDemo(name: String) {
    val firstProperty = "Первое свойство: $name".also(::println)

    init {
        println("Первый блок инициализации: ${name}")
    }

    val secondProperty = "Второе свойство: ${name.length}".also(::println)

    init {
        println("Второй блок инициализации: ${name.length}")
    }
}