fun main() {
    val initOrderDemo = InitOrderDemo("hello")

    val helloWorld = object {
        val hello = "Hello"
        val world = "World!"
        // тип анонимных объектов - Any, поэтому `override` необходим в `toString()`
        override fun toString() = "$hello, $world"
    }
    println(helloWorld)
    val ab: A = object : A(1), B {
        override val y = 15
    }
    println(ab)
}

open class A(x: Int) {
    public open val y: Int = x
    override fun toString() = "y=$y"
}

interface B { /*...*/ }

