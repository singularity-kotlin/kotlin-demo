val scan = java.util.Scanner(System.`in`)
fun main(args: Array<String>){
    val phrase = scan.nextLine().lowercase().replace("[,—.! ]".toRegex(), "")
    println(phrase)
    var low=0
    var high=phrase.length-1
    var isPalindrome=true
    while(low<=high) {
        if(phrase[low]!=phrase[high]) {
            isPalindrome=false
            println(false)
            break
        }
        low++
        high--
    }
    if(isPalindrome)
        println(true)
    //println true or false
}